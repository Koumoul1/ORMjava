     ----------------------------------------
    |                                        |
    | "Begin at the beginning,"              |
    |                                        |
    |      the King say, very gravely,       |
    |                                        |
    |  "and go on till you come to the end:  |
    |   then stop."                          |
    |                                        |
    |       -The King, Alice in Wonderland   |
     ----------------------------------------

This ORM give priority to a coherent SQL structure. 
Therefore, we made the deliberate choice to NOT implement inheritance.

----- https://en.wikipedia.org/wiki/Composition_over_inheritance -----

This ORM implement composition. To do so, we use a hash function, 
so each peculiar object is saved only once in the database.

     ----------------------------------------
    |                                        |
    | Computer are like Old Testament Gods;  |
    | lots of rules and no mercy.            |
    |                                        |
    |                 -Joseph Campbell       |
     ----------------------------------------


Rules of the ORM:

#1 One declared class is equivalent to one table.

#2 Every Classes that use the ORM CRUD function's MUST extend the 
   abstract class Savable (and only this class).

#3 Every delcared attribute of a Savable Object MUST BE Savable.

#4 and the end of the constructor, YOU HAVE TO call: setHash(calcHash());

#5 For deleting and updating you have to implementations, 
   the default implementation only delete/update the object in his table,
   the second implementation recursively delete/update the object plus 
   ALL the declared attribute that compose this object.

#6 If you delete something, It's your job to check that it is not 
   involved in any dependency.
   In the next version (if there is one) of the ORM 
   we will implement a way to keep track of the dependencies between objects.
                                          
                                             .
                                            /!\
                                           /!!!\ WARNING
                                          /!!!!!\

In this ORM we use hash function's but we DON'T HANDLE COLLISIONS.
To avoid them, pray.


DATASTRUCTURE:

                  Client
                  +---------------+
                  |               |
                  | x firstname   |
                  | x lastname    |
                  | x email       |
                  | o city        |
                  | x address     |
                  | x newletter   |
                  |               |
                  |               |
                  |               |
                  |               |
               +--+---------------+
City           |
+--------------+
|              |
| x name       |
| x mayor      |
| x population |
| x budget     |
|              |
|              |
|              |
|              |
|              |
|              |
+--------------+
