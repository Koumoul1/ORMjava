import java.lang.reflect.Method;
import java.util.ArrayList;
import java.lang.reflect.Field;


public abstract class Savable {

	public int	hash;
	
	Savable() {
		setHash(calcHash());
	}
	
	public int calcHash() {
		ArrayList<Object> values = new ArrayList<Object>();
		Class<?> clazz 				= this.getClass();
		Field[] fields 	= clazz.getDeclaredFields();
		ArrayList<Method> getters 	= ORM.Util.gettersOfClass(clazz);
	
		try {
		
			for (int i = 0; i < getters.size(); i++) {
				// if is a declared Field, the function save called on the object
				if (ORM.Util.isDeclaredClass(fields[i].getType()))
					values.add(((Savable) getters.get(i).invoke(this)).getHash());

				else if (!fields[i].getName().equals("hash"))
					values.add(getters.get(i).invoke(this));
			}
		} catch (Exception e) {System.out.println(e);}
		
		return	values.hashCode();
	}
	
	public int getHash() {
		setHash(this.calcHash());
		return this.hash;
	}
	
	public void setHash(int hash) {
		this.hash = hash;
	}
}
