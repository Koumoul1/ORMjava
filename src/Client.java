import java.util.ArrayList;

public class Client extends Savable {
	
	
	private String 	firstname;
	private String 	lastname;
	private String 	email;
	private City	city;
	private String 	address;
	private boolean	newsletter;

	
	Client() {}
	
	Client(String firstname, String lastname, String email, City city, String address, boolean newsletter) {
		setFirstname(firstname);
		setLastname(lastname);
		setEmail(email);
		setCity(city);
		setAddress(address);
		setNewsletter(newsletter);
		setHash(calcHash());
	}

	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public String getEmail() {
		return email;
	}
	public String getAddress() {
		return address;
	}
	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public boolean isNewsletter() {
		return newsletter;
	}
	public void setFirstname(String firstName) {
		this.firstname = firstName;
	}
	public void setLastname(String lastName) {
		this.lastname = lastName;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}
	
	public String toString() {
		String toString = new String(getFirstname() + " " + getLastname() + " " + getEmail());
		return toString;
	}
	
}
