import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public class ORM {
	
	
	// The function save take an object, 
	// if this object is not in the DB it's saved and the hash is returned, 
	// else only the hash is returned 
	public static int save(Savable obj) throws Exception {
		String 				statement 	= null;
		int					hash;
		ArrayList<Object> 	values 		= new ArrayList<Object>();
		ArrayList<Method> 	getters 	= new ArrayList<Method>();
		Field[] 			fields;
		Class<?> 			clazz 		= Util.getParents(obj.getClass())[0];
		
		hash = obj.calcHash();
		// Get parent Value
		// recusive call with "void save(Object Obj)" if the type on the field is a declared field
		getters = Util.gettersOfClass(clazz);
		fields 	= clazz.getDeclaredFields();
		
		for (int i = 0; i < getters.size(); i++) {
			// if is a declared Field, We check if the hash is in the table, if it's not we call save on the object.
			if (Util.isDeclaredClass(fields[i].getType())) {
				Savable att_obj = (Savable)getters.get(i).invoke(obj);
				int att_hash = att_obj.getHash();
				// if the hash is in the database, just add the hash to the table. Else we call save on the object.
				if (Util.isInDatabase(att_hash, att_obj.getClass().getName()))
					values.add(att_hash);
				else
					values.add(save(att_obj));
			}
			else
				values.add(getters.get(i).invoke(obj));
		}
		// add the hash to the object's values, by convention the hash is save at the last column of the SQL table.
		values.add(hash);
		// If the hash IS NOT in the database, the object is save.
		if (!Util.isInDatabase(hash, clazz.getName())) {
			statement = Util.forgeInsertStatement(clazz, obj);
			//Send the statement to sql manager
			sql.sendPrepStatement(statement, values);
		}
		return hash;
	}

	public static void delete(Savable obj) {
		String		table 	= obj.getClass().getName(); //by convention the class name is equivalent to the table name.
		int			hash		= obj.calcHash(); 		// the hash of the object
		
		String statement = "DELETE FROM " + table + " WHERE hash=" + hash;
		sql.sendUpdateStatement(statement);
	}
	
	public static void delete_recrusively(Object obj) {
		
	}
	
	public static void update(Object oldObj, Object newObj) {
		
	}
	
	public static void update_recursively(Savable oldObj, Savable newObj) {
		
	}
	
	public static void getById(int idObj, Class<?> c) {
		
	}



	public static class Util {
		
		private static boolean isInDatabase(int hash, String className) {
			String statement = new String("SELECT * FROM " + className + " WHERE hash=" + hash +";");
			String response = sql.sendStatement(statement);
			if (response.equals(""))
				return false;
			else
				return true;
		}
		
		private static String forgeInsertStatement(Class<?> parent, Object obj) {
			StringBuilder build = new StringBuilder();
			String statement;
			Field[] fields = parent.getDeclaredFields();
			

			build.append("INSERT INTO " + parent.getName().replace("businessLogic.", "") + " (");

			try {
			// AJOUT DU NOM DES CHAMPS
			for (int i = 0; i < fields.length; i++)
				build.append(fields[i].getName() + ", ");

			build.append(" hash");
			build.append(")");
			build.append(" VALUES (");
			for (int i = 0; i < fields.length; i++)
				build.append("?,");

			build.append("?)");
			statement = build.toString();

			System.out.println(statement);
			return statement;
			
			} catch (Exception e) {
				System.out.println("ERROR");
				return "";
			}
		}

		public static ArrayList<Object> getObjectValues(Object obj) {
			Class<?>[] parents = Util.getParents(obj.getClass());
			ArrayList<Object> values 	= new ArrayList<Object>();
			ArrayList<Method> getters 	= new ArrayList<Method>();
			ArrayList<Field> fields 	= new ArrayList<Field>();
			
			for (Class<?> parent: parents) {
				getters.addAll(gettersOfClass(parent));
				fields.addAll(new ArrayList<Field>(Arrays.asList(parent.getDeclaredFields())));
			}
			for (int i = 0; i < getters.size(); i++) {
				if (!fields.get(i).getName().equals("id"))
					try {
						values.add(getters.get(i).invoke(obj));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			return values;
		}

		public static ArrayList<Object> getParentValues(Class<?> parent, Object obj) {
			ArrayList<Object> values 	= new ArrayList<Object>();
			ArrayList<Method> getters 	= new ArrayList<Method>();
			Field[] fields;
			
			getters = gettersOfClass(parent);
			fields = parent.getDeclaredFields();
			
			for (int i = 0; i < getters.size(); i++) {
				if (!fields[i].getName().equals("id")) {
					try {
						values.add(getters.get(i).invoke(obj));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			return values;
		}

		public static ArrayList<Method> gettersOfClass(Class<?> emp) {
			Field[] fields = emp.getDeclaredFields();
			ArrayList<Method> getters = new ArrayList<Method>();
			String[] gettersName = buildGettersName(fields);

			for (int i = 0; i < gettersName.length; i++) {
				Method m = null;
				try {
					m = emp.getMethod(gettersName[i]);
				} catch (NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
				getters.add(m);
			}
			return getters;
		}

		public static String[] buildGettersName(Field[] fields) {
			ArrayList<String> gettersNameAL = new ArrayList<String>();
			String[] gettersName;

			for (int i = 0; i < fields.length; i++) {
				String fieldName = fields[i].getName();
				fieldName = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
				if (fields[i].getType() == boolean.class)
					gettersNameAL.add("is" + fieldName);
				else
					gettersNameAL.add("get" + fieldName);
			}
			gettersName = gettersNameAL.toArray(new String[gettersNameAL.size()]);
			return gettersName;
		}

		public static boolean isDeclaredClass(Class<?> c) {
			if (c == String.class || c == int.class || c == boolean.class || c == Integer.class) {
				return false;
			} else
				return true;
		}

		public static Class<?>[] getParents(Class<?> emp) {
			Class<?> buff;
			ArrayList<Object> parents = new ArrayList<Object>();
			parents.add(emp);

			while (emp != Object.class && emp.getSuperclass() != Object.class) {
				buff = emp.getSuperclass();
				emp = buff;
				parents.add(buff);
			}
			return parents.toArray(new Class[parents.size()]);
		}

		public static int getId_obj(Object obj) {
			Method getId_obj = null;
			try {
				getId_obj = obj.getClass().getMethod("getId_obj");
			} catch (NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int id_obj = 0;
			try {
				id_obj = (int) getId_obj.invoke(obj);
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return id_obj;
		}
	}
	
}
