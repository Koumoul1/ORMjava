
public class City extends Savable {

	private String 	name;
	private String	mayor;
	private int		population;
	private int		budget;
	
	City() {};
	
	City(String name, String mayor, int population, int budget) {
		setName(name);
		setMayor(mayor);
		setPopulation(population);
		setBudget(budget);
		setHash(calcHash());
	}
	
	public String 	getName() {
		return name;
	}
	public void 	setName(String name) {
		this.name = name;
	}
	public String 	getMayor() {
		return mayor;
	}
	public void 	setMayor(String mayor) {
		this.mayor = mayor;
	}
	public int 		getPopulation() {
		return population;
	}
	public void 	setPopulation(int population) {
		this.population = population;
	}
	public int 		getBudget() {
		return budget;
	}
	public void 	setBudget(int budget) {
		this.budget = budget;
	}

	public int 		getHash() {
		return hash;
	}

	public void 	setHash(int hash) {
		this.hash = hash;
	}
}
