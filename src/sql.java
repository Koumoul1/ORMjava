
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class sql {
    static String url;
    static String user;
    static String password;
    
    static Connection cnx;
    
    sql(){};
    
    public sql(String url, String user, String password) {
    	setUrl(url);
    	setUser(user);
    	setPassword(password);
    	
    	setConnection(connect(getUrl(), getUser(), getPassword()));
    }

    public Connection connect(String url, String user, String password) {
        
    	Connection cnx = null;
		try {
			cnx = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return cnx;
    }
    
    public static void sendPrepStatement(String prepStatement, ArrayList<Object> valeurs) throws SQLException {
    	PreparedStatement pstm = null;
    	try {
    		pstm = cnx.prepareStatement(prepStatement);
    		int i = 1;
        	for (Object valeur: valeurs) {
        		pstm.setObject(i, valeur);
        		i++;
        	}
        	int inserted = pstm.executeUpdate();
        	System.out.println(inserted + "ligne(s) inserée(s)");
    	} catch (SQLException ex) {
    		  System.out.println("SQL Error in 'sql.sendPrepStatement': " + ex.getMessage());
    	} finally {
    		pstm.close();
    	}
    	
    }
    
    public static String sendStatement(String statement) {
    	Statement 		stm 	= null;
    	ResultSet		res 	= null;
    	StringBuilder 	sbuild 	= new StringBuilder();
    	
    	try {
    		stm = cnx.createStatement();
    		res = stm.executeQuery(statement);
    		
    		while (res.next()) {
    			sbuild.append(res.getString(1));
    			sbuild.append("\n");
    		}
    		
    	} catch (SQLException ex) {
    		System.out.println("SQL Error in 'sql.sendStatement': " + ex.getMessage());
    	}
    	return sbuild.toString();
    }

    public static int sendUpdateStatement(String statement) {
    	Statement 		stm 	= null;
    	int				error 	= 1;
    	
    	try {
    		stm = cnx.createStatement();
    		error = stm.executeUpdate(statement);
    		
    	} catch (SQLException ex) {
    		System.out.println("Error - " + ex.getMessage());
    	}
    	return error;
    }
    
    public static ArrayList<Object> getRow(String table, int id_obj) throws SQLException {
    	Statement 	stm = null;
    	ResultSet 	res = null;
    	int 		columnNumber;
    	ArrayList<Object> list = new ArrayList<Object>();
    	
		stm = cnx.createStatement();
		
		// get the number of row of the table
		res = stm.executeQuery("select count(*) from INFORMATION_SCHEMA.COLUMNS WHERE table_name = '"+ table +"'");
		res.first();
		columnNumber = res.getInt(1);
		
		//get the row by obj_id
		res = stm.executeQuery("SELECT * FROM " + table + " WHERE id_obj=" + id_obj);
		res.first();
		// add the info into an array list
    	for (int i = 3; i < columnNumber + 1; i++)
    		list.add(res.getObject(i));
    	
    	return list;
    	
    }

    public static ArrayList<String> getColumnName(String table) throws SQLException {
    	Statement 	stm = null;
    	ResultSet 	res = null;
    	ArrayList<String> columnName = new ArrayList<String>();
    	
		stm = cnx.createStatement();    	
		res = stm.executeQuery("select * from INFORMATION_SCHEMA.COLUMNS WHERE table_name = '"+ table +"'");
		
		res.first();
		res.next();
		while (res.next()) {
			columnName.add(res.getString(4));
		}
		return columnName;
    }
    
    public static ArrayList<String> getTablesName() {
       	Statement 		stm 	= null;
    	ResultSet		res 	= null;
    	ArrayList<String> tablesName = new ArrayList<String>();
    	
    	try {
    		stm = cnx.createStatement();
    		res = stm.executeQuery("SHOW TABLES");
    		
    		while (res.next()) {
    			tablesName.add(res.getString(1));
    		}
    		
    	} catch (SQLException ex) {
    		System.out.println("Error - " + ex.getMessage());
    	}
    	return tablesName;
    }
  
    public static Object			getCell(String table, int id_obj, String column) throws SQLException {
    	Statement 	stm = null;
    	ResultSet 	res = null;
    	
		stm = cnx.createStatement();
    	
    	res = stm.executeQuery("SELECT " + column + " FROM " + table + " WHERE id_obj=" + id_obj);
		res.first();
		return res.getObject(1);
    }
    
    
    public static String getUrl() {
		return url;
	}
	public static String getUser() {
		return user;
	}
	public static String getPassword() {
		return password;
	}
	public static void setUrl(String url) {
		sql.url = url;
	}
	public static void setUser(String user) {
		sql.user = user;
	}
	public static void setPassword(String password) {
		sql.password = password;
	}
    public static void setConnection(Connection cnx) {
    	sql.cnx = cnx;
    }
	
}
